library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity counter is
    Port ( upcount : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           clock : in  STD_LOGIC ;
			  counter_out: out INTEGER RANGE 0 TO 7);
end counter;

architecture Behavioral of counter is
	SIGNAL cnt : INTEGER RANGE 0 TO 8 ;
begin

	PROCESS(clock)
		
	BEGIN
		if rising_edge(clock) then
			IF(reset = '1' ) THEN
				cnt <= 0 ;
			ELSIF(upcount = '1') THEN
				cnt <= cnt + 1 ;
			END IF ;
		end if ;
	END PROCESS;
	
	counter_out <= cnt ;
		
end Behavioral;