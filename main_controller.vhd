library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity main_controller is
    Port(   clock 	: in  STD_LOGIC;
			reset	: in  STD_LOGIC;
			start 	: in STD_LOGIC;
			data_in : in STD_LOGIC;
           
			p1 : out  STD_LOGIC;
			p2 : out  STD_LOGIC;
			p3 : out  STD_LOGIC;
			p4 : out  STD_LOGIC;
          
			empty 		 : out  STD_LOGIC_VECTOR(3 downto 0);
			outofservice : out  STD_LOGIC;
			busy 		 : out STD_LOGIC;
			done 		 : out  STD_LOGIC;
        
			money : in  STD_LOGIC;
			type1 : in  STD_LOGIC;
			type2 : in  STD_LOGIC;
			type3 : in  STD_LOGIC;
			type4 : in  STD_LOGIC);
end main_controller;

architecture Behavioral of main_controller is

	COMPONENT pr_module port ( clock : in STD_LOGIC;
			reset 	: in STD_LOGIC;
			start 	: in  STD_LOGIC;
			data_in : in  STD_LOGIC;
				
			data_ready : out STD_LOGIC;
			data_out   : out  STD_LOGIC_VECTOR(7 DOWNTO 0));
	END COMPONENT ;
	FOR g1 : pr_module USE ENTITY work.protocol(Behavioral);
	
	type state_type is (idle, outService, waitingForUser, requestProcess, adminProcess);
	signal cur_state, next_state : state_type ;
	signal product_num1 : STD_LOGIC_VECTOR(2 downto 0);
	signal product_num2 : STD_LOGIC_VECTOR(2 downto 0);
	signal product_num3 : STD_LOGIC_VECTOR(2 downto 0);
	signal product_num4 : STD_LOGIC_VECTOR(2 downto 0);
	signal data_ready 	: std_logic ;
	signal data 		: STD_LOGIC_VECTOR(7 downto 0);
	begin 
	
	g1 : pr_module PORT MAP(clock, reset, start, data_in, data_ready, data);
	
	PROCESS(clock)
	BEGIN
		IF(reset = '1') THEN
			cur_state <= idle ;
		ELSIF(clock'event and clock = '1') THEN	
			cur_state <= next_state ;
		END IF;
	END PROCESS ;
	
	PROCESS(cur_state, start, money, data_ready)
	BEGIN
		CASE cur_state IS
			when idle =>
				if (data_ready = '1') then
					next_state <= adminProcess ;
				elsif (money = '1') then
					next_state <= outService ;
				else
					next_state <= idle ;
				end if ;
			when adminProcess =>
				if (start = '1') then
					next_state <= idle;
				else
					next_state <= waitingForUser ;
				end if ;
			when waitingForUser =>
				if (money = '1') then
					next_state <= requestProcess ;
				else
					next_state <= waitingForUser ;
				end if ;
			when requestProcess =>
				if (start = '1') then
					next_state <= idle ;
				else
					next_state <= waitingForUser ;
				end if ;
			when outService =>
				next_state <= idle ;
		END CASE ;
	END PROCESS ;
	
	PROCESS(cur_state, start, money, data_ready)
	BEGIN
			p1 <= '0' ; p2 <= '0'; p3 <= '0' ; p4 <= '0' ;
			empty <= "0000" ;
			outofservice <= '0' ;
			done <= '0' ;
			busy <= '0' ;
			
		CASE cur_state	IS
			when idle | waitingForUser =>
				p1 <= '0' ; p2 <= '0'; p3 <= '0' ; p4 <= '0' ;
				empty <= "0000" ;
				outofservice <= '0' ;
				done <= '0' ;
				busy <= '0' ;
			when adminProcess =>
				CASE data(1 downto 0) IS
					when "00" =>
						product_num1 <= data(4 downto 2);
					when "01" =>
						product_num2 <= data(4 downto 2);
					when "10" =>
						product_num3 <= data(4 downto 2);
					when "11" =>
						product_num4 <= data(4 downto 2);
					when others =>
						product_num4 <= data(4 downto 2);
				END CASE;
				busy <= '1' ;
				done <= '1' ;
				
			-- after money was inserted
			when requestProcess =>
				busy <= '1' ;
				if(type1 = '1') then
					if (product_num1 = "000") then 
						empty <= "0001" ;
					else 
						p1 <= '1' ;
						product_num1 <= product_num1 -1 ;
					end if ;
				elsif(type2 = '1') then	
					if (product_num2 = "000") then 
						empty <= "0010" ;
					else 
						p2 <= '1' ;
						product_num2 <= product_num2 -1 ;
					end if ;
				elsif(type3 = '1') then	
					if (product_num3 = "000") then 
						empty <= "0100" ;
					else 
						p3 <= '1' ;
						product_num3 <= product_num3 -1 ;
					end if ;
				elsif(type4 = '1') then	
					if (product_num4 = "000") then 
						empty <= "1000" ;
					else
						p4 <= '1' ;
						product_num4 <= product_num4 - 1 ;
					end if ;
				end if ;
			when outService =>
				outofservice <= '1' ;
		END CASE ;
	END PROCESS ;
	
end Behavioral;

