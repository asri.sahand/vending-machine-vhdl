library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity protocol is
    Port ( 	clock 	: in STD_LOGIC ;
			reset 	: in STD_LOGIC ;
			start 	: in  STD_LOGIC;
			data_in : in  STD_LOGIC;
			
			data_ready : out STD_LOGIC;
			data_out   : out  STD_LOGIC_VECTOR(7 DOWNTO 0));
end protocol;

architecture Behavioral of protocol is

	COMPONENT d_path PORT ( 
			cnt_out 	: out integer range 0 to 8 ;
			clock 		: in STD_LOGIC ;
			data_in 	: in  STD_LOGIC;
			data_out 	: out  STD_LOGIC_VECTOR(7 downto 0);
			data_re  	: in  STD_LOGIC;
			load_data 	: in STD_LOGIC ;
			upcount 	: in  STD_LOGIC;
			rst_counter : in  STD_LOGIC);
	END COMPONENT ;
	
	COMPONENT cntrlr PORT ( counter : in  INTEGER RANGE 0 TO 8;
			start 		: in  STD_LOGIC;
			data_ready 	: out  STD_LOGIC;
			upcount 	: out  STD_LOGIC;
			rst_counter : out  STD_LOGIC;
			load_data 	: out STD_LOGIC ;
			clock 		: in  STD_LOGIC;
			reset 		: in STD_LOGIC);
	END COMPONENT ;
	
	FOR g1 : d_path USE ENTITY work.protocol_datapath(Behavioral);
	FOR g2 : cntrlr USE ENTITY work.protocol_cntrl(Behavioral);
	
	signal ld_data , tmp ,upcnt , rst_cntr : STD_LOGIC ;
	signal cnt : integer range 0 to 8 ;
	
begin
	g1 : d_path PORT MAP (cnt ,clock , data_in , data_out , tmp , ld_data , upcnt , rst_cntr);
	g2 : cntrlr PORT MAP (cnt , start , tmp , upcnt , rst_cntr , ld_data , clock , reset);
	data_ready <= tmp ;
end Behavioral;

