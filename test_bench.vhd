LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY test_bench IS
END test_bench;
 
ARCHITECTURE behavior OF test_bench IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT main_controller
    PORT(
         clock : IN  std_logic;
         reset : IN  std_logic;
         start : IN  std_logic;
         data_in : IN  std_logic;
         p1 : OUT  std_logic;
         p2 : OUT  std_logic;
         p3 : OUT  std_logic;
         p4 : OUT  std_logic;
         empty : OUT  std_logic_vector(3 downto 0);
         outofservice : OUT  std_logic;
         busy : OUT  std_logic;
         done : OUT  std_logic;
         money : IN  std_logic;
         type1 : IN  std_logic;
         type2 : IN  std_logic;
         type3 : IN  std_logic;
         type4 : IN  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clock : std_logic := '0';
   signal reset : std_logic := '0';
   signal start : std_logic := '0';
   signal data_in : std_logic := '0';
   signal money : std_logic := '0';
   signal type1 : std_logic := '0';
   signal type2 : std_logic := '0';
   signal type3 : std_logic := '0';
   signal type4 : std_logic := '0';

 	--Outputs
   signal p1 : std_logic;
   signal p2 : std_logic;
   signal p3 : std_logic;
   signal p4 : std_logic;
   signal empty : std_logic_vector(3 downto 0);
   signal outofservice : std_logic;
   signal busy : std_logic;
   signal done : std_logic;

   -- Clock period definitions
   constant clock_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: main_controller PORT MAP (
          clock => clock,
          reset => reset,
          start => start,
          data_in => data_in,
          p1 => p1,
          p2 => p2,
          p3 => p3,
          p4 => p4,
          empty => empty,
          outofservice => outofservice,
          busy => busy,
          done => done,
          money => money,
          type1 => type1,
          type2 => type2,
          type3 => type3,
          type4 => type4
        );

   -- Clock process definitions
   clock_process :process
   begin
		clock <= '0';
		wait for clock_period/2;
		clock <= '1';
		wait for clock_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      reset <= '1' ;
      wait for 100 ns;	
			reset <= '0' ;
			wait for clock_period;	
			--data_in <= '1';
			start <= '1' ;
			wait for clock_period;
			start <= '0' ;
			
			--wait for 8*clock_period
			wait for 3*clock_period;
			data_in <= '1';
			wait for 5*clock_period;
			money <= '1' ;
			type2 <= '1' ;
			
			wait for clock_period;
			money <= '0' ;
			wait for clock_period;
			money <= '1' ;
			wait for clock_period;
			money <= '0' ;
			type2 <= '0' ;
			wait for clock_period;
			data_in <= '0' ;
			start <= '1' ;
			wait for clock_period;
			start <= '0' ;
			wait for 8*clock_period;
      wait;
   end process;

END;
