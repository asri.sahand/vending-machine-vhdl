library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity protocol_cntrl is
    Port( 	counter 	: in  INTEGER RANGE 0 TO 8;
			start 		: in  STD_LOGIC;
			data_ready 	: out  STD_LOGIC;
			upcount 	: out  STD_LOGIC;
			rst_counter : out  STD_LOGIC;
			load_data 	: out STD_LOGIC ;
			clock 		: in  STD_LOGIC;
			reset 		: in STD_LOGIC);
end protocol_cntrl;

architecture Behavioral of protocol_cntrl is
	type state_type is (idle , data_rec);
	signal cur_state , next_state : state_type ;
begin
	PROCESS(clock)
	BEGIN
		IF(reset = '1') THEN
			cur_state <= idle ;
		ELSIF(clock'event and clock = '1') THEN	
			cur_state <= next_state ;
		END IF;
	END PROCESS ;
	
	PROCESS(cur_state, start, counter)
	BEGIN
		CASE cur_state IS
			when idle =>
				if (start = '1') then
					next_state <= data_rec ;
				else next_state <= idle ;
				end if ;
			when data_rec =>
				if (counter < 8) then
					next_state <= data_rec ;
				elsif (counter = 8) then
					next_state <= idle ;
				end if ;
		END CASE ;
	END PROCESS ;
	
	PROCESS(counter , cur_state)
	BEGIN
			upcount <= '0' ;
			rst_counter <='0' ;
			data_ready <= '0' ;
			load_data <= '0' ;
		CASE cur_state	IS
			when idle =>
				upcount <= '0' ;
				rst_counter <='0' ;
				data_ready <= '0' ;
				load_data <= '0' ;				
			when data_rec => 
				if (counter < 8) then
					upcount <= '1' ;
					load_data <= '1' ;
				elsif (counter = 8) then
					data_ready <= '1' ;
					rst_counter <= '1' ;
				end if;
		END CASE ;
	END PROCESS ;

end Behavioral;

