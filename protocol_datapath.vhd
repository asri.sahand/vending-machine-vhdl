library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity protocol_datapath is
    Port( cnt_out : out INTEGER RANGE 0 to 8 ;
		  clock 	  : in STD_LOGIC ;
		  data_in 	  : in  STD_LOGIC;
          data_out 	  : out  STD_LOGIC_VECTOR(7 downto 0);
          data_re  	  : in  STD_LOGIC;
		  load_data   : in STD_LOGIC ;
          upcount 	  : in  STD_LOGIC;
          rst_counter : in  STD_LOGIC);
end protocol_datapath;

architecture Behavioral of protocol_datapath is
	
	COMPONENT cntr PORT ( upcount : in  STD_LOGIC;
			reset : in  STD_LOGIC;
			clock : in  STD_LOGIC ;
			counter_out: out INTEGER RANGE 0 TO 7);
	END COMPONENT ;
	
	FOR g1 :cntr USE ENTITY work.counter(Behavioral);
	
	signal temp : STD_LOGIC_VECTOR(7 downto 0);
	signal cnt : INTEGER RANGE 0 TO 8 ;
	
begin
	g1 : cntr PORT MAP(upcount , rst_counter , clock , cnt);
	cnt_out <= cnt ;
	PROCESS(clock)
	BEGIN
		if(load_data = '1')then
			temp(cnt) <= data_in ;
		end if ;
	END PROCESS ;
	
	PROCESS(clock)
	BEGIN
		if(data_re = '1')then
			data_out <= temp ;
		end if ;
	END PROCESS ;

end Behavioral;

